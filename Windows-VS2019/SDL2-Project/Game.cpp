#include "Game.h"
#include "TextureUtils.h"

#include "Player.h"
#include "NPC.h"
#include "Bullet.h"
#include "DamagePowerup.h"
#include "Vector2f.h"
#include "AABB.h"
#include <windows.h>

//for printf
#include <cstdio>

// for exceptions
#include <stdexcept>

Game::Game() 
{
    gameWindow = nullptr;
    gameRenderer = nullptr;

    backgroundTexture = nullptr;
    player = nullptr;
    npc = nullptr;
    npc1 = nullptr;
    npc2 = nullptr;
    damagepowerup = nullptr;

    keyStates = nullptr;

    quit = false;

    NPCdead = false;
    NPC1dead = false;
    NPC2dead = false;
}

void Game::init()
{
     gameWindow = SDL_CreateWindow("Sweet Shot!",   // Window title
                            SDL_WINDOWPOS_UNDEFINED, // X position
                            SDL_WINDOWPOS_UNDEFINED, // Y position
                            WINDOW_WIDTH,            // width
                            WINDOW_HEIGHT,           // height               
                            SDL_WINDOW_SHOWN);       // Window flags  
    
    if(gameWindow != nullptr)
    {
        // if the window creation succeeded create our renderer
        gameRenderer = SDL_CreateRenderer(gameWindow, 0, 0);

        if(gameRenderer == nullptr)
        {
          throw std::runtime_error("Error - SDL could not create renderer\n");          
        }
    }
    else
    {
        // could not create the window, so don't try and create the renderer. 
        throw std::runtime_error("Error - SDL could not create Window\n");
    }
    
    // Track Keystates array
	keyStates = SDL_GetKeyboardState(NULL);

    // Create background texture from file, optimised for renderer 
    backgroundTexture = createTextureFromFile("assets/images/background_horror.png", gameRenderer);

    if(backgroundTexture == nullptr)
        throw std::runtime_error("Background image not found\n");

    //setup player
    player = new Player();
    player->init(gameRenderer);
    player->setGame(this);

    npc = new NPC();
    npc->init(gameRenderer);
    npc->setGame(this);
    npc->respawn(WINDOW_HEIGHT, WINDOW_WIDTH);

    npc1 = new NPC();
    npc1->init(gameRenderer);
    npc1->setGame(this);
    npc1->respawn(WINDOW_HEIGHT, WINDOW_WIDTH);

    npc2 = new NPC();
    npc2->init(gameRenderer);
    npc2->setGame(this);
    npc2->respawn(WINDOW_HEIGHT, WINDOW_WIDTH);

    damagepowerup = new DamagePowerup();
    damagepowerup->init(gameRenderer);
    damagepowerup->setGame(this);

    SDL_SetRenderDrawColor(gameRenderer, 0, 0, 0, 255);
    // Colour provided as red, green, blue and alpha (transparency) values (ie. RGBA)
}

Game::~Game()
{
    //Clean up!
    delete player;
    player = nullptr;  

    // npcs
    delete npc;
    npc = nullptr;

    delete npc1;
    npc1 = nullptr;

    delete npc2;
    npc2 = nullptr;

    delete npc3;
    npc3 = nullptr;

    delete npc4;
    npc4 = nullptr;

    delete damagepowerup;
    damagepowerup = nullptr;

    for (vector<Bullet*>::iterator it = bullets.begin() ; it != bullets.end();)
    {   
        delete *it;
        *it = nullptr;
        it = bullets.erase(it);
    } 
   
    SDL_DestroyTexture(backgroundTexture);
    backgroundTexture = nullptr;
    
    SDL_DestroyRenderer(gameRenderer);
    gameRenderer = nullptr;
    
    SDL_DestroyWindow(gameWindow);
    gameWindow = nullptr;   
}

void Game::draw()
{
    // 1. Clear the screen
    SDL_RenderClear(gameRenderer);

    // 2. Draw the scene
    SDL_RenderCopy(gameRenderer, backgroundTexture, NULL, NULL);

    player->draw(gameRenderer);
    npc->draw(gameRenderer);
    damagepowerup->draw(gameRenderer);

    if (NPC1spawned == true)
    {
        npc1->draw(gameRenderer);
    }

    if (NPC2spawned == true)
    {
        npc2->draw(gameRenderer);
    }

    for(int i = 0; i < bullets.size(); i++)
        bullets[i]->draw(gameRenderer);

    // 3. Present the current frame to the screen
    SDL_RenderPresent(gameRenderer);

}

void Game::update(float timeDelta)
{
    player->update(timeDelta);
    damagepowerup->update(timeDelta);

    // check for npc dead
    if (npc->isDead()) 
    {
        NPCdead = true;
    }

    if (npc1->isDead())
    {
        NPC1dead = true;
    }

    if (npc2->isDead())
    {
        NPC2dead = true;
    }

    // respawn if npcs are dead
    if (NPCdead == true && NPC1spawned == false)
    {
        npc->respawn(WINDOW_HEIGHT, WINDOW_WIDTH);
        npc1->respawn(WINDOW_HEIGHT, WINDOW_WIDTH);
        NPCdead = false;
        NPC1spawned = true;
    }

    if (NPC1dead == true && NPC2spawned == false)
    {
        npc->respawn(WINDOW_HEIGHT, WINDOW_WIDTH);
        npc1->respawn(WINDOW_HEIGHT, WINDOW_WIDTH);
        npc2->respawn(WINDOW_HEIGHT, WINDOW_WIDTH);
        NPCdead = false;
        NPC1dead = false;
        NPC2spawned = true;
    }

    if (NPCdead == true && NPC1dead == true && NPC2dead == true)
    {
        npc->respawn(WINDOW_HEIGHT, WINDOW_WIDTH);
        npc1->respawn(WINDOW_HEIGHT, WINDOW_WIDTH);
        npc2->respawn(WINDOW_HEIGHT, WINDOW_WIDTH);
        NPCdead = false;
        NPC1dead = false;
        NPC2dead = false;
    }

    if (NPC1spawned)
    {
        npc1->update(timeDelta);
    }

    if (NPC2spawned)
    {
        npc2->update(timeDelta);
    }

    npc->update(timeDelta);

    for (vector<Bullet*>::iterator it = bullets.begin(); it != bullets.end();)
    {
        if((*it)->hasExpired())
        {
            delete *it;
            *it = nullptr;
            it = bullets.erase(it);
        }
        else
        {
            ++it;
        }
    }

    for (vector<Bullet*>::iterator it = bullets.begin() ; it != bullets.end(); ++it)
    {
        (*it)->update(timeDelta);
    }

    collisionDetection();
    NPCCollisionDetection();
    DMGPowerupCollisionDetection();

}

void Game::processInputs()
{
    SDL_Event event;

    // Handle input 
    if( SDL_PollEvent( &event ))  // test for events
    { 
        switch(event.type) 
        { 
            case SDL_QUIT:
                quit = true;
            break;

            // Key pressed event
            case SDL_KEYDOWN:
                switch (event.key.keysym.sym)
                {
                case SDLK_ESCAPE:
                    quit = true;
                    break;
                }
            break;

            // Key released event
            case SDL_KEYUP:
                switch (event.key.keysym.sym)
                {
                case SDLK_ESCAPE:
                    //  Nothing to do here.
                    break;
                }
            break;
            
            default:
                // not an error, there's lots we don't handle. 
                break;    
        }
    }

    // Process Inputs - for player 
    player->processInput(keyStates);
}

Player* Game::getPlayer() 
{
    return player;
}

void Game::runGameLoop()
{
    // Timing variables
    unsigned int currentTimeIndex; 
    unsigned int timeDelta;
    float timeDeltaInSeconds;
    unsigned int prevTimeIndex;

    // initialise preTimeIndex
    prevTimeIndex = SDL_GetTicks();

    // Game loop
    while(!quit) // while quit is not true
    { 
	    // Calculate time elapsed
        // Better approaches to this exist 
        //- https://gafferongames.com/post/fix_your_timestep/
        currentTimeIndex = SDL_GetTicks();	
        timeDelta = currentTimeIndex - prevTimeIndex; //time in milliseconds
        timeDeltaInSeconds = timeDelta * 0.001f;
        	
        // Store current time index into prevTimeIndex for next frame
        prevTimeIndex = currentTimeIndex;

        // Process inputs
        processInputs();

        // Update Game Objects
        update(timeDeltaInSeconds);

        //Draw stuff here.
        draw();

        SDL_Delay(1);
    }
}

void Game::createBullet(Vector2f* position, Vector2f* velocity)
{
    Bullet* bullet = new Bullet();

    bullet->init(gameRenderer, position, velocity);

    if (hasDamagePowerup == true)
    {
        bullet->doubleDamage();
    }

    bullets.push_back(bullet);
}

void Game::collisionDetection()
{
    for (vector<Bullet*>::iterator it = bullets.begin(); it != bullets.end();)
    {
        // player score
        if ((*it)->getAABB()->intersects(npc->getAABB()) && !npc->isDead())
        {
            if (hasDamagePowerup == true)
            {
                player->addScore(20);
            }
            else player->addScore(10);
            npc->takeDamage((*it)->getDamage());
            printf("Player Score: %d\n", player->getScore());
        }
        if ((*it)->getAABB()->intersects(npc1->getAABB()) && !npc1->isDead())
        {
            if (hasDamagePowerup == true)
            {
                player->addScore(20);
            }
            else player->addScore(10);
            npc1->takeDamage((*it)->getDamage());
            printf("Player Score: %d\n", player->getScore());
        }
        if ((*it)->getAABB()->intersects(npc2->getAABB()) && !npc2->isDead())
        {
            if (hasDamagePowerup == true)
            {
                player->addScore(20);
            }
            else player->addScore(10);
            npc2->takeDamage((*it)->getDamage());
            printf("Player Score: %d\n", player->getScore());
        }
        if ((*it)->getAABB()->intersects(npc->getAABB()) || (*it)->getAABB()->intersects(npc1->getAABB()) || (*it)->getAABB()->intersects(npc2->getAABB()))
        {
            delete* it;
            *it = nullptr;
            it = bullets.erase(it);
        }
        else
        {
            ++it;
        }
    }
}

void Game::NPCCollisionDetection()
{
    if ((player)->getAABB()->intersects(npc->getAABB()) && !npc->isDead())
    {
        quit = true;
    }
    if ((player)->getAABB()->intersects(npc1->getAABB()) && !npc1->isDead() && NPC1spawned)
    {
        quit = true;
    }
    if ((player)->getAABB()->intersects(npc2->getAABB()) && !npc2->isDead() && NPC2spawned)
    {
        quit = true;
    }
}

void Game::DMGPowerupCollisionDetection()
{
    if ((player)->getAABB()->intersects(damagepowerup->getAABB()))
    {
        hasDamagePowerup = true;
        damagepowerup->respawn(WINDOW_HEIGHT, WINDOW_WIDTH);
        printf("DAMAGE POWER-UP COLLECTED - You now deal 2x damage.");
    }
}
