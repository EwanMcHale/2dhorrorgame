#ifndef GAME_H_
#define GAME_H_

#include "SDL2Common.h"

#include <vector>
using std::vector;

class Player;
class NPC;
class Bullet;
class DamagePowerup;
class Vector2f;

class Game
{
private:
    // Declare window and renderer objects
    SDL_Window* gameWindow;
    SDL_Renderer* gameRenderer;
    Vector2f* position;

    // Background texture
    SDL_Texture* backgroundTexture;

    //Declare Player
    Player* player;

    //Declare NPC
    NPC* npc;
    NPC* npc1;
    NPC* npc2;

    DamagePowerup* damagepowerup;

    // Bullets
    vector<Bullet*> bullets;

    // Window control 
    bool            quit;

    // Keyboard
    const Uint8* keyStates;

    // Game loop methods. 
    void processInputs();
    void update(float timeDelta);
    void draw();

public:
    // Constructor 
    Game();
    ~Game();

    bool NPCdead = false;
    bool NPC1dead = false;
    bool NPC2dead = false;

    bool NPC1spawned = false;
    bool NPC2spawned = false;

    bool hasDamagePowerup = false;

    // Methods
    void init();
    void runGameLoop();
    Player* getPlayer();
    void createBullet(Vector2f* position, Vector2f* velocity);
    void collisionDetection();
    void NPCCollisionDetection();
    void DMGPowerupCollisionDetection();

    // Public attributes
    static const int WINDOW_WIDTH = 800;
    static const int WINDOW_HEIGHT = 600;
};

#endif