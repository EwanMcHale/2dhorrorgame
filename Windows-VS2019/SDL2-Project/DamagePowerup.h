#ifndef DamagePowerup_H_
#define DamagePowerup_

#include "SDL2Common.h"
#include "Sprite.h"

// Forward declerations
// improve compile time. 
class Vector2f;
class Animation;
class Game;


class DamagePowerup : public Sprite
{
private:

    // Animation state
    int state;

    // Sprite information
    static const int SPRITE_HEIGHT = 50;
    static const int SPRITE_WIDTH = 50;

    // Need game
    Game* game;

public:
    DamagePowerup();
    ~DamagePowerup();

    enum NPCState { IDLE = 0, DEAD };

    void init(SDL_Renderer* renderer);
    void update(float timeDeltaInSeconds);

    // Update 'things' ai related

    int getCurrentAnimationState();

    void setGame(Game* game);

    void respawn(const int MAX_HEIGHT, const int MAX_WIDTH);

};

#endif