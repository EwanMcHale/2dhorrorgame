#include "DamagePowerup.h"
#include "Animation.h"
#include "Vector2f.h"
#include "TextureUtils.h"
#include "Game.h"
#include "Player.h"
#include "AABB.h"

#include <stdexcept>
#include <string>

using std::string;

/**
 * NPC
 *
 * Constructor, setup all the simple stuff. Set pointers to null etc.
 *
 */
DamagePowerup::DamagePowerup() : Sprite()
{
    state = IDLE;
    speed = 300.0f;

    targetRectangle.w = SPRITE_WIDTH;
    targetRectangle.h = SPRITE_HEIGHT;
}

/**
 * init
 *
 * Function to populate an animation structure from given paramters.
 *
 * @param renderer Target SDL_Renderer to use for optimisation.
 * @exception Throws an exception on file not found or out of memory.
 */
void DamagePowerup::init(SDL_Renderer* renderer)
{
    //path string
    string path("assets/images/damage-powerup.png");

    //postion
    Vector2f position(700.0f, 50.0f);

    // Call sprite constructor
    Sprite::init(renderer, path, 6, &position);

    animations[IDLE]->init(1, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 0);
    animations[DEAD]->init(1, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 0);

    animations[DEAD]->setLoop(false);

    for (int i = 0; i < maxAnimations; i++)
    {
        animations[i]->setMaxFrameTime(0.1f);
    }

    aabb = new AABB(this->getPosition(), SPRITE_HEIGHT, SPRITE_WIDTH);
}

/**
 * ~NPC
 *
 * Destroys the NPC and any associated
 * objects
 *
 */
DamagePowerup::~DamagePowerup()
{

}

void DamagePowerup::update(float dt)
{
    velocity->setX(0.0f);
    velocity->setY(0.0f);

    Sprite::update(dt);
}

void DamagePowerup::setGame(Game* game)
{
    this->game = game;
}

int DamagePowerup::getCurrentAnimationState()
{
    return state;
}

void DamagePowerup::respawn(const int MAX_HEIGHT, const int MAX_WIDTH)
{
    Vector2f randomPostion;

    int doubleWidth = MAX_WIDTH;
    int doubleHeight = MAX_HEIGHT;

    // get a random number between 0 and 
    // 2 x screen size. 
    int xCoord = rand() % ((700 - 100) + 1) + 100;
    int yCoord = rand() % ((550 - 50) + 1) + 50;

    // if its on screen move it off. 
    if (xCoord < MAX_WIDTH)
        xCoord *= -1;

    if (yCoord < MAX_HEIGHT)
        yCoord *= -1;

    randomPostion.setX(xCoord);
    randomPostion.setY(yCoord);

    this->setPosition(&randomPostion);
}
